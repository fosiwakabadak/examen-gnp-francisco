export const viewInicio = {
    titulo: 'Lista de empleados',
    boton: {
        recargar: 'Recargar lista',
        agregar: 'Agregar empleado'
    },
    alertas: {
        exito: 'Se obtuvieron los empleados correctamente.',
        error: 'No se obtuvieron los empleados, intentelo de nuevo por favor.'
    }
}

export const erroresHttp = {
    errorProtocolo: 'Ocurrió un error con la conexión, por favor, verifica la conexión de internet e intentalo de nuevo'
};

export const formularioEditarEmpleado = {
    botonEditar: 'Editar',
    input: {
        nombre: {
            error: 'Introduce un nombre válido.',
            placeholder: 'Ingresa tu nombre.',
            label: 'Nombre Completo:'
        },
        edad: {
            error: 'Introduce una edad válida.',
            placeholder: 'Ingresa tu edad.',
            label: 'Edad:'
        },
        salario: {
            error: 'Introduce un salario válido.',
            placeholder: 'Ingresa tu salario.',
            label: 'Salario:'
        }
    } 
};

export const formularioAgregarEmpleado = {
    botonAgregar: 'Agregar',
    input: {
        nombre: {
            error: 'Introduce un nombre válido.',
            placeholder: 'Ingresa tu nombre.',
            label: 'Nombre Completo:'
        },
        edad: {
            error: 'Introduce una edad válida.',
            placeholder: 'Ingresa tu edad.',
            label: 'Edad:'
        },
        salario: {
            error: 'Introduce un salario válido.',
            placeholder: 'Ingresa tu salario.',
            label: 'Salario:'
        }
    }
};

export const detalleEmpleadoComponent = {
    botonPdf: {
        nombre: 'Generar PDF'
    }
}