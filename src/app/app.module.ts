import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { InicioComponent } from './pages/inicio/inicio.component';
import { ListaComponent } from './components/lista/lista.component';
import { NzMessageService } from 'ng-zorro-antd/message';
import { EditarEmpleadoComponent } from './components/formularios/editar-empleado/editar-empleado.component';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { AgregarEmpleadoComponent } from './components/formularios/agregar-empleado/agregar-empleado.component';
import { DetalleEmpleadoComponent } from './components/detalle-empleado/detalle-empleado.component';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzAlertModule } from 'ng-zorro-antd/alert';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ListaComponent,
    EditarEmpleadoComponent,
    AgregarEmpleadoComponent,
    DetalleEmpleadoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzTableModule,
    NzButtonModule,
    NzSkeletonModule,
    NzGridModule,
    NzDrawerModule,
    NzToolTipModule,
    NzCardModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzInputNumberModule,
    NzPopoverModule,
    NzAlertModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US}, {provide: NzMessageService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
