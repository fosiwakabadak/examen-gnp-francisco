import { TestBed } from '@angular/core/testing';

import { ObtenerEmpleadosService } from './obtener-empleados.service';

describe('ObtenerEmpleadosService', () => {
  let service: ObtenerEmpleadosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerEmpleadosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
