import { Injectable } from '@angular/core';
import { MiddlewareService } from '../middleware/middleware.service';
import { Observable } from 'rxjs';
import { EmpleadoRegistro, EmpleadosResponse } from '../../interfaces/empleados';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ObtenerEmpleadosService {

  constructor(private http: MiddlewareService) { }

  obtenerEmpleados(): Observable<Array<EmpleadoRegistro>> {
    const url = 'https://dummy.restapiexample.com/api/v1/employees';
    return this.http.get(url).pipe(map((response: EmpleadosResponse) => response.data) );
  }
}
