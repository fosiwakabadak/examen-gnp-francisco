import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { erroresHttp } from '../../../globals';
import { ErrorServicio } from '../../interfaces/middleware';

@Injectable({
  providedIn: 'root'
})
export class MiddlewareService {


  constructor(protected http: HttpClient) {}

  get(url: string): Observable<any> {
    return this.http
        .get(url)
        .pipe(catchError( (error) => throwError(error) ) )
  }
}
