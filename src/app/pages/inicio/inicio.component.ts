import { Component, OnInit } from '@angular/core';
import { Columna, EmitClicList } from '../../interfaces/lista';
import { columnas } from './fixtures';
import { EmpleadoRegistro } from '../../interfaces/empleados';
import { viewInicio } from '../../../globals';
import { ObtenerEmpleadosService } from '../../services/obtenerEmpleados/obtener-empleados.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  columnas: Array<Columna> = columnas;
  rows: Array<EmpleadoRegistro> = null;
  textos = viewInicio;
  cargando = false;
  visible = false;
  tipoFormulario: string;
  tituloDrawer: string;
  empleado: EmpleadoRegistro;

  constructor(
    private empleadosService: ObtenerEmpleadosService,
    private alerta: NzMessageService
  ) { }

  ngOnInit(): void {
    this.obtenerEmpleados();
  }

  obtenerEmpleados() {
    this.rows = null;
    this.cargando = true;
    this.empleadosService.obtenerEmpleados().subscribe(
      (empleados: Array<EmpleadoRegistro>) => {
        this.cargando = false;
        this.rows = empleados;
        this.ordenarEmpleados();
        this.alerta.create('success', this.textos.alertas.exito);
      }, () => {
        this.cargando = false,
        this.alerta.create('error', this.textos.alertas.error);
      }
    )
  }

  alDarClicBoton(row: EmitClicList) {
    this.empleado = row.row;
    if (row.columna.key !== 'eliminar') {
      this.open(row.columna.key);
    } else {
      this.eliminarEmpleado(this.empleado);
    }
  }

  close(): void {
    this.visible = false;
  }

  open(tipoFormulario: string): void {
    this.tipoFormulario = tipoFormulario;
    this.tituloDrawer = tipoFormulario === 'editar' ? 'Editar empleado' :
      tipoFormulario === 'eliminar' ? 'Eliminar empleado' : 
      tipoFormulario === 'agregar' ? 'Agregar empleado' :
      'Detalle del empleado';
    this.visible = true;
  }

  alEditar(empleadoEditado: EmpleadoRegistro) {
    this.close();
    let newRows: Array<EmpleadoRegistro> = [];
    this.rows.forEach((empleado: EmpleadoRegistro) => {
      newRows.push(empleado.id === empleadoEditado.id ? empleadoEditado : empleado);
    })
    this.rows = newRows;
  }

  alAgregar(empleadoAgregado: EmpleadoRegistro) {
    let id = this.calcularId();
    this.close();
    this.cargando = true;
    empleadoAgregado.id = id;
    this.rows.push(empleadoAgregado);
    this.ordenarEmpleados();
  }

  ordenarEmpleados() {
    let newRows: Array<EmpleadoRegistro> = [];
    this.rows.forEach((empleado: EmpleadoRegistro) => newRows[empleado.id - 1] = empleado );
    this.rows = newRows;
    this.cargando = true;
    setTimeout(() => this.cargando = false, 100);
  }

  calcularId(): number {
    let id = 0;
    let ids: Array<number> = [];
    this.rows.forEach((empleado: EmpleadoRegistro) => ids.push(empleado.id));

    ids.forEach((currentId: number, index: number) => {
      index++;
      if (id === 0) {
        id = currentId !== index ? index : 0;
      }
    })
    id = id !== 0 ? id : ids.length + 1;

    return id;
  }

  eliminarEmpleado(empleadoEliminado: EmpleadoRegistro) {
    this.rows = this.rows.filter((empleado: EmpleadoRegistro) => empleado.id !== empleadoEliminado.id )
    this.ordenarEmpleados();
  }

}
