import { Columna } from '../../interfaces/lista';
import { EmpleadoRegistro } from '../../interfaces/empleados';

export const columnas: Array<Columna> = [
    {
        nombre: 'Id',
        key: 'id',
    },
    {
        nombre: 'Nombre del empleado',
        key: 'employee_name',
    },
    {
        nombre: 'Edad',
        key: 'employee_age',
    },
    {
        nombre: 'Salario',
        key: 'employee_salary',
        pipe: 'currency'
    },
    {
        nombre: '',
        key: 'detalle',
        boton: {
            type: 'primary',
            shape: 'circle',
            icon: 'user',
            tooltip: true,
            nombre: 'Detalle del empleado'
        }
    },
    {
        nombre: '',
        key: 'editar',
        boton: {
            type: 'dashed',
            shape: 'circle',
            icon: 'edit',
            tooltip: true,
            nombre: 'Editar empleado'
        }
    },
    {
        nombre: '',
        key: 'eliminar',
        boton: {
            type: 'primary',
            shape: 'circle',
            icon: 'delete',
            danger: true,
            tooltip: true,
            nombre: 'Eliminar empleado',
            popover: true,
            popoverTitle: '¿Eliminar al Empleado?'
        }
    }
]