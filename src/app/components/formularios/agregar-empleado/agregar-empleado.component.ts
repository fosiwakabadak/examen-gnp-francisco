import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmpleadoRegistro } from '../../../interfaces/empleados';
import { formularioAgregarEmpleado } from '../../../../globals';

@Component({
  selector: 'app-agregar-empleado',
  templateUrl: './agregar-empleado.component.html',
  styleUrls: ['./agregar-empleado.component.css']
})
export class AgregarEmpleadoComponent implements OnInit {
  @Output() alAgregar: EventEmitter<EmpleadoRegistro> = new EventEmitter();
  formEmpleado: FormGroup;
  textos = formularioAgregarEmpleado;
  datosForm: EmpleadoRegistro;
  formatterDollar = (value: number) =>  value ? `$ ${value.toFixed(2)}` : '' ;
  formatterNumber = (value: number) => value ? Math.round( value ) : '';
  parserDollar = (value: string) =>  value ? value.replace('$ ', '') : '';

  ngOnInit(): void {
    this.crearFormulario();
  }

  crearFormulario() {
    this.formEmpleado = new FormGroup({
      employee_name: new FormControl('', Validators.required),
      employee_age: new FormControl(null, Validators.required),
      employee_salary: new FormControl('', Validators.required),
    })
    this.formEmpleado.valueChanges.subscribe((values: EmpleadoRegistro) => this.datosForm = values);
  }

  alEnviarFormulario() {
    const empleadoAgregado: EmpleadoRegistro = {
      employee_age: this.datosForm.employee_age,
      employee_name: this.datosForm.employee_name,
      employee_salary: this.datosForm.employee_salary,
      id: null
    }
    this.alAgregar.emit(empleadoAgregado);
  }
}
