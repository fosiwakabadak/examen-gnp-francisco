import { Component, Input, OnInit, Output , EventEmitter} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmpleadoRegistro } from '../../../interfaces/empleados';
import { formularioEditarEmpleado } from '../../../../globals';

@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.css']
})
export class EditarEmpleadoComponent implements OnInit {
  @Input() empleado: EmpleadoRegistro;
  @Output() alEditar: EventEmitter<EmpleadoRegistro> = new EventEmitter();
  formEmpleado: FormGroup;
  textos = formularioEditarEmpleado;
  datosForm: EmpleadoRegistro;
  formatterDollar = (value: number) =>  value ? `$ ${value.toFixed(2)}` : '' ;
  formatterNumber = (value: number) => value ? Math.round( value ) : '';
  parserDollar = (value: string) =>  value ? value.replace('$ ', '') : '';

  constructor() { }

  ngOnInit(): void {
    this.crearFormulario();
  }

  crearFormulario() {
    this.formEmpleado = new FormGroup({
      employee_name: new FormControl(this.empleado.employee_name, Validators.required),
      employee_age: new FormControl(this.empleado.employee_age, Validators.required),
      employee_salary: new FormControl(this.empleado.employee_salary, Validators.required),
    })
    this.formEmpleado.valueChanges.subscribe((values: EmpleadoRegistro) => this.datosForm = values);
  }

  alEnviarFormulario() {
    const empleadoEditado: EmpleadoRegistro = {
      employee_age: this.datosForm.employee_age,
      employee_name: this.datosForm.employee_name,
      employee_salary: this.datosForm.employee_salary,
      id: this.empleado.id
    }
    this.alEditar.emit(empleadoEditado);
  }

}
