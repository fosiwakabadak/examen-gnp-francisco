import { Component, OnInit, Input } from '@angular/core';
import { EmpleadoRegistro } from '../../interfaces/empleados';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { detalleEmpleadoComponent } from '../../../globals';

@Component({
  selector: 'app-detalle-empleado',
  templateUrl: './detalle-empleado.component.html',
  styleUrls: ['./detalle-empleado.component.css']
})
export class DetalleEmpleadoComponent {
  @Input() empleado:  EmpleadoRegistro;
  textos = detalleEmpleadoComponent
  imgDefault = './../../../assets/img/usr.png'

  generarPDF() {
    let DATA = document.getElementById('empleado');
    
    html2canvas(DATA).then(canvas => {
        let fileWidth = 208;
        let fileHeight = canvas.height * fileWidth / canvas.width;

        const FILEURI = canvas.toDataURL('image/png')
        let PDF = new jsPDF('p', 'mm', 'a4');
        let position = 0;
        PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight)

        PDF.save('empleado.pdf');
    });     
  }

}
