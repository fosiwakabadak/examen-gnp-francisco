import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Columna, EmitClicList } from '../../interfaces/lista';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent {
  @Input() rows: Array<object>;
  @Input() columnas: Array<Columna>;
  @Input() rowsPorPagina: number = 5;
  @Output() alDarClicFila: EventEmitter<object> = new EventEmitter();
  @Output() alDarClicBoton: EventEmitter<EmitClicList> = new EventEmitter();

  alDarClicF(row: object, columna: Columna) {
    this.alDarClicFila.emit({ row, columna });
  }

  alDarClicBot(row: object, columna: Columna) {
    this.alDarClicBoton.emit({ row, columna });
  }
}
