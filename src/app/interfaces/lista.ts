export interface Columna {
    nombre: string;
    key: string;
    boton?: BotonConfig;
    pipe?: string; 
}

export interface BotonConfig {
    icon?: string;
    type?: string;
    shape?: string;
    danger?: boolean;
    nombre?: string;
    tooltip?: true;
    popoverTitle?: string;
    popover?: boolean;
}

export interface EmitClicList {
    row: any,
    columna: Columna
}