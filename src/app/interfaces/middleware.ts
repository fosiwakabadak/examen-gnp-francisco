export interface ErrorServicio {
    error?: any;
    mensajeError: string;
    responseStatus: number;
}