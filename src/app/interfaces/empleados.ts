export interface EmpleadosResponse {
    data: Array<EmpleadoRegistro>;
}

export interface EmpleadoRegistro {
    employee_age: number;
    employee_name: string;
    employee_salary: number;
    id: number;
    profile_image?: string;
}